import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 21.04.2017.
 */
public class textPages extends locators {

    public textPages(WebDriver driver){this.driver = driver;}

    public void setContent(){
        driver.findElement(content).click();
    }

    public void setListOfTextPages(){
        driver.findElement(listOfTextPages).click();
    }
    public void setChekAll(){
        driver.findElement(selectAll).click();
    }
    public void setUnpublish(){
        driver.findElement(UnpublishAll).click();
    }

    public void deleteAll(){
        this.setContent();
        this.setListOfTextPages();
        this.setChekAll();
        this.setUnpublish();
    }

}
