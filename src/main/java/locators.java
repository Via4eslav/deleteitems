import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 21.04.2017.
 */
public class locators {

    public WebDriver driver;

    //Login page locators

    By login = By.id("login");
    By password = By.id("password");
    By enterButton = By.id("enterLink");

    //Text Pages locators
    By content = By.xpath(".//*[@id='mCSB_1_container']/div/ul/li[2]/a");
    By listOfTextPages = By.xpath("html/body/div[1]/div[1]/div[1]/div[1]/div/div/ul/li[2]/ul/li[1]/a");
    By selectAll = By.cssSelector(".bs-tooltip.ckbxWrap.liTipLink>input");
    By UnpublishAll = By.cssSelector("html/body/div[1]/div[2]/div/div[4]/div/div/a[3]");

}
