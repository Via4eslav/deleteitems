import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 22.04.2017.
 */
public class login extends locators {
    public login(WebDriver driver){
        this.driver = driver;
    }

    public void setLogin(String strLogin){
        driver.findElement(login).sendKeys(strLogin);
    }

    public void setPassword(String strPassword){
        driver.findElement(password).sendKeys(strPassword);
    }

    public void setEnterButton(){
        driver.findElement(enterButton).click();
    }

    public void logInCms(String strLogin, String strPassword){
        this.setLogin(strLogin);
        this.setPassword(strPassword);
        this.setEnterButton();
    }
}
